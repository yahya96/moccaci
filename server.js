var express = require("express");
var bodyParser = require("body-parser");
var sql = require("mssql");
var app = express(); 

// Body Parser Middleware
app.use(bodyParser.json()); 

app.use(bodyParser.urlencoded({ extended: false }))

//CORS Middleware
app.use(function (req, res, next) {
    //Enabling CORS 
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, contentType,Content-Type, Accept, Authorization");
    next();
});

//Setting up server
 var server = app.listen(process.env.PORT || 5000, function () {
    var port = server.address().port;
    console.log("App now running on port", port);
 });
//Initiallising connection string
var dbConfig = {
    user: "sa",
    password: "1234",
    server: "DESKTOP-J5IL6JN",
    database: "ypsserver",
    port: 1433,
  
};


var  executeQuery = function(res, query){             
  sql.connect(dbConfig, function (err) {
      if (err) {   
                  console.log("Error while connecting database :- " + err);
                  res.send(err);
               }
               else {
                      // create Request object
                      var request = new sql.Request();
                      // query to the database
                      request.query(query, function (err, tes) {
                        if (err) {
                                   console.log("Error while querying database :- " + err);
                                   res.send(err);
                                   sql.close();
                                  }
                                  else {
                                    res.send(tes);
                                    sql.close();
                                  }
                            });
                    }
   });           
}


app.get("/api/regional", function(req , res){
  var query = "select * from mregional";
  executeQuery (res, query);
});

app.get("/api/regional/:id", function(req , res){
  var query = "select * from mregional  WHERE noid= " + req.params.id;
  executeQuery (res, query);
});

app.get("/api/find_regional/:nama", function(req , res){
  var str=req.params.nama.split(',')
  var query = "select * from mregional  WHERE "+str[0]+" like '%" +str[1]+"%' ";
  executeQuery (res, query);
});

//POST API
app.post("/api/regional", function(req , res){
  var query = "INSERT INTO mregional (noid,nama) VALUES ((select CONVERT(int,max(noid)+1) from mregional),'"+ req.body.nama +"')";
  executeQuery (res, query);
  console.log(req.body);
  // console.log(req.body.recordsets[0].nama);
});

//PUT API
app.put("/api/regional/:id", function(req , res){
  var query = "UPDATE mregional SET Nama= '" + req.body.nama  +  "'  WHERE noid= " + req.params.id;
  executeQuery (res, query);
});

// DELETE API
app.delete("/api/regional/:id", function(req , res){
  var query = "DELETE FROM mregional WHERE noid=" + req.params.id;
  executeQuery (res, query);
});

module.exports = app;