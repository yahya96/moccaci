
var assert = require('chai').assert
  , foo = 'bar'
  , beverages = { tea: [ 'chai', 'matcha', 'oolong' ] };

describe('Test Coba', function() {
    it('check 1', function() {
        assert.typeOf(foo, 'string'); // without optional message
    });
    it('check 2', function() {
        assert.typeOf(foo, 'string', 'foo is a string'); // with optional message
    });
    it('check 3', function() {
        assert.equal(foo, 'bar', 'foo equal `bar`');
    });
    it('check 4', function() {
       assert.lengthOf(foo, 3, 'foo`s value has a length of 3');
    });
    it('check 5', function() {
        assert.lengthOf(beverages.tea, 3, 'beverages has 3 types of tea');
    });
})

