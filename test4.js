
var chai = require('chai'),
	expect = chai.expect,
	chaiHttp = require('chai-http');
chai.use(chaiHttp);


describe('Test group', function() {
    var host = "http://localhost:5000"
    var path = "/api/regional";

    it('Test Menambah', function(done) {
        chai
            .request(host)
            .post(path)
            .set('content-type', 'application/x-www-form-urlencoded')
            .send({nama: 'test'})
            .end(function(error, response, body) {
                if (error) {
                    done(error);
                } else {
                    done();
                }
            });
    });

    var path2 = "/api/regional/12";
    it('Test Edit', function(done) {
        chai
            .request(host)
            .put(path2)
            .set('content-type', 'application/x-www-form-urlencoded')
            .send({nama: 'cobba'})
            .end(function(error, response, body) {
                if (error) {
                    done(error);
                } else {
                	expect(response).to.have.status(200);
                    done();
                }
            });
    });

    it('Test Menampilkan', function(done) {
        chai
            .request(host)
            .get(path)
			.end(function(err, res) {
			    expect(res).to.have.status(200);
			    done();                               // <= Call done to signal callback end
			  });
    });
});