const { Builder, Capabilities } = require("selenium-webdriver");
const chrome = require("selenium-webdriver/chrome")
var capabilities = Capabilities.chrome();
//To avoid InsecureCertificateError for selenium4-aplha5
capabilities.setAcceptInsecureCerts(true);
capabilities.set("browserVersion", "67");
capabilities.set("platformName", "Windows XP");
(async function helloSelenium() {
    let driver = new Builder()
        .usingServer("http://selenium__standalone-chrome:4444/wd/hub")   
        .withCapabilities(capabilities)
        .build();
    try {
        await driver.get('http://www.google.com');
    }    
    finally {       
        await driver.quit();
    }
})(); 